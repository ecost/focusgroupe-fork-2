/**
 * `static/js/app.js`
 *
 * Worked on by: David Yan (Team 1, Group A)
 *   2014-06-10: Created AngularJS shell for other groups to use
 *   2014-06-11: Enabled HTML5Mode now that Django serves static asset properly
 *   2014-06-22: Added routes for Login
 *   2014-07-02: Refactored the way login/logout works (now via event emission)
 *   2014-07-06: Added routes for course editing
 *   2014-07-09: Added routes for profile
 *
 * Worked on by: Jaime To (Team 1, Group A)
 *   2014-06-25: Added routes for Create Instructor
 *   2014-06-25: Added routes for Create Course
 *   2014-07-17: Added Angular-ui component
 */

(function() {

  // Allow the app to use native route (/route) instead of fake route (/#route)
  this.config(function($locationProvider) {
    $locationProvider.html5Mode(true);
  });

  /**
   * Defines every route of the app
   *
   * The `controller` attribute needs to be a defined controller in a JS file in
   * static/js/controllers
   *
   * The `templateUrl` attribute needs to be a HTML template file in
   * static/partials
   *
   * The `resolve` attribute are a set of promises that must return succeed
   * before the route is allow to be accessed, and once all promises fulfilled,
   * the data from the promise is extracted and passed onto the controller,
   * named by the key of the resolve element.
   * See:
   * http://www.html5rocks.com/en/tutorials/es6/promises/
   * https://docs.angularjs.org/api/ngRoute/provider/$routeProvider
   */
  this.config(function($routeProvider) {
    $routeProvider.when('/', {
      controller: 'IndexCtrl',
      templateUrl: '/static/partials/index.html'
    });

    $routeProvider.when('/404', {
      controller: '404Ctrl',
      templateUrl: '/static/partials/404.html'
    });

    $routeProvider.when('/login', {
      controller: 'LoginCtrl',
      templateUrl: '/static/partials/login.html'
    });

    $routeProvider.when('/profile', {
      controller: 'ProfileCtrl',
      templateUrl: '/static/partials/profile.html'
    });

    $routeProvider.when('/create-instructor', {
      controller: 'InstructorCtrl',
      templateUrl: '/static/partials/create-instructor.html'
    });

    $routeProvider.when('/course/:id', {
      controller: 'CourseCtrl',
      templateUrl: '/static/partials/course.html',
      resolve: {
        course: function($route, Course) {
          return Course.getCourse($route.current.params.id);
        }
      }
    });

    $routeProvider.when('/create-course', {
      controller: 'CreateCourseCtrl',
      templateUrl: '/static/partials/create-course.html',
      resolve: {
        courses: function(Course) {
          return Course.getCourses();
        }
      }
    });

    $routeProvider.when('/group-b-route', {
      controller: 'GroupBCtrl',
      templateUrl: 'static/partials/group-b-tmpl.html'
    });

    $routeProvider.when('/group-c-route', {
      controller: 'GroupCCtrl',
      templateUrl: 'static/partials/group-c-tmpl.html'
    });

    $routeProvider.when('/group-d-route', {
      controller: 'GroupDCtrl',
      templateUrl: 'static/partials/group-d-tmpl.html'
    });

    $routeProvider.when('/group-e-route', {
      controller: 'GroupECtrl',
      templateUrl: 'static/partials/group-e-tmpl.html'
    });

    $routeProvider.otherwise({
      redirectTo: '/404'
    });
  });

  // All API endpoints are prefixed with `api`
  // For example: localhost:8000/api/accounts/endpoint
  this.config(function(RestangularProvider) {
    RestangularProvider.setBaseUrl('/api');
  });

  // A skeleton HTTP interceptor, for possible future use
  // For example, when an API hit is made, display global an AJAX spinner, and
  // when the hit is finished, reenable the UI
  this.config(function($httpProvider) {
    $httpProvider.interceptors.push(function($q) {
      return {
        request: function(config) {
          return config;
        },
        requestError: function(rejection) {
          return $q.reject(rejection);
        },
        response: function(response) {
          return response;
        },
        responseError: function(rejection) {
          return $q.reject(rejection);
        }
      };
    });
  });

  // Configuration options for when Angular initializes the app.
  this.run(function($rootScope, $location, User, Restangular, Cookie) {

    // Takes the `csrftoken` cookie that Django sets and adds it to the header
    function updateHeader() {
      Restangular.setDefaultHeaders({
        'X-CSRFToken': Cookie.get('csrftoken')
      });
    }

    // Expose the logout functionality app-wide, to be used in any tmpl context
    $rootScope.logout = User.logout;

    // Event emission for login and logout. Basically, whenever either event
    // occurs, update the `csrftoken` in the API endpoint hit headers.
    $rootScope.$on('$login', updateHeader);
    $rootScope.$on('$logout', updateHeader);
    updateHeader(); // update the header when the app first runs

    // Below are skeleton events for route changesm for possible future use
    // For example, when the user changes routes, we can show some sort of
    // animation and what not
    $rootScope.$on('$routeChangeStart', function() {
    });

    $rootScope.$on('$routeChangeError', function() {
      // If any resolve objects fail, redirect to 404
      $location.path('/404');
    });

    $rootScope.$on('$routeChangeSuccess', function() {
    });
  });

// App dependencies are passed as an arugment into the above IIFE. See:
// http://en.wikipedia.org/wiki/Immediately-invoked_function_expression
}).call(angular.module('app', [
  'restangular', // Restangular is a helpful lib for hitting REST APIs
  'ngRoute', // ngRoutes is what enables us to have custom routes (URLs)
  'ngSanitize', // For possible future use when displaying User-created content
  'controllers', // Our controllers, defined in static/js/controllers
  'services', // Our services/models, defined in static/js/services
  'ui.bootstrap' // Angular directives based on Bootstrap
]));
