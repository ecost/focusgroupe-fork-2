/**
 * `static/js/services/Accounts.js`
 *
 * Worked on by: David Yan (Team 1, Group A)
 *   2014-06-10: Created the Accounts controller as part of the AngularJS shell
 *   2014-06-20: Added controller for login page
 *   2014-06-30: Added controller for course creation page
 *   2014-07-02: Added controller for instructor creation page
 *   2014-07-06: Added controller for course page
 *   2014-07-09: Added controller for profile page
 */

(function() {

  /**
   * The login controller.
   *
   * @route '/login'
   */
  this.controller('LoginCtrl', function($scope, User) {
    $scope.checkLoggedIn = User.checkLoggedIn;
    $scope.login = User.login;
    $scope.logout = User.logout;
  });

  /**
   * The profile controller.
   *
   * @route '/profile'
   */
  this.controller('ProfileCtrl', function($scope, User) {
    $scope.changePassword = function(old_password, new_password) {
      User.changePassword(old_password, new_password).then(function() {
        // Success
      }, function() {
        // Failure
      });
    }
  });

  /**
   * The create course controller.
   *
   * @route '/create-course'
   */
  this.controller('CreateCourseCtrl', function($scope, Course, courses) {
    $scope.create = function(name, emails) {
      Course.create(name, emails).then(function() {
        $scope.courses = Course.getCourses().$object;
      });
    }

    $scope.courses = courses;
  });

  /**
   * The course controller.
   *
   * @route '/course'
   */
  this.controller('CourseCtrl', function($scope, Course, course) {
    $scope.course = course;

    function update(emails) {
      Course.update(course.id, emails).then(function() {
        $scope.course.student_emails = emails;
      });
    }

    $scope.add = function(emails) {
      emails = _.union(emails, $scope.course.student_emails);
      update(emails);
    };

    $scope.remove = function(email) {
      var emails = _.without($scope.course.student_emails, email);
      update(emails);
    }
  });

  /**
   * The instructor controller.
   *
   * @route '/create-course'
   */
  this.controller('InstructorCtrl', function($scope, User, Instructor) {
    $scope.create = Instructor.create;
  });

}).call(angular.module('controllers'));
