(function() {

  this.controller('GroupDCtrl', function($scope, GroupD) {
    console.log('GroupDCtrl');
    $scope.hello = 'Hello from GroupDCtrl';

    GroupD.testCall().then(function(data) {
      if (data.test === "success") {
        $scope.testCall = true;
      }
    });
  });

}).call(angular.module('controllers'));
