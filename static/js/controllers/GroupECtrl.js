(function() {

  this.controller('GroupECtrl', function($scope, GroupE) {
    console.log('GroupECtrl');
    $scope.hello = 'Hello from GroupECtrl';

    GroupE.testCall().then(function(data) {
      if (data.test === "success") {
        $scope.testCall = true;
      }
    });
  });

}).call(angular.module('controllers'));
