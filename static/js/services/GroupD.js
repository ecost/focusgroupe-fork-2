(function() {

  this.factory('GroupD', function(Restangular) {

    function testCall() {
      return Restangular.one('endpoint.json').get();
    }

    return {
      testCall: testCall
    };
  });

}).call(angular.module('services'));
